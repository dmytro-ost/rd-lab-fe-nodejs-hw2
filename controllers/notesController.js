const {
  User,
} = require('../models/userModel');
const {
  Note,
} = require('../models/noteModels');


const getNotes = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  const {
    limit,
    offset,
  } = req.note;


  const notes = await Note.find({
    userId: req.user._id,
  }, {
    _id: 1,
    userId: 1,
    completed: 1,
    text: 1,
    createdDate: 1,
  }).limit(limit).skip(offset);


  if (!notes) {
    return res.status(400).json({
      message: `Bad request. Note not found!`,
    });
  }


  return res.status(200).json({
    'notes': notes,
  });
};


const addNote = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  const note = new Note({
    userId: user._id,
    text: req.body.text,
  });

  await note.save((err) => {
    if (err) {
      return res.status(400).json({
        message: err.message,
      });
    }
    return res.status(200).json({
      message: 'Note saved successfully!',
    });
  });
};


const getNoteById = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  const note = await Note.findOne({
    _id: req.params.id,
    userId: req.user._id,
  }, {
    _id: 1,
    userId: 1,
    completed: 1,
    text: 1,
    createdDate: 1,
  });

  if (!note) {
    return res.status(400).json({
      message: `Bad request. Note not found!`,
    });
  }

  return res.status(200).json({
    'note': note,
  });
};


const checkNoteById = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  Note.findOne({
    _id: req.params.id,
    userId: req.user._id,
  }, (err, note) => {
    if (err || !note) {
      return res.status(400).json({
        message: `Can't update COMPLETED field in DB. ${err}`,
      });
    }
    note.completed = !note.completed;
    note.save((err, result) => {
      if (err) {
        return res.status(400).json({
          message: `Can't update COMPLETED field in DB. ${err}`,
        });
      }
      return res.status(200).json({
        message: `Success`,
      });
    });
  });
};


const updateNoteById = async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  Note.findOne({
    _id: req.params.id,
    userId: req.user._id,
  }, (err, note) => {
    if (err || !note) {
      return res.status(400).json({
        message: `Can't update NOTE in DB. ${err}`,
      });
    }

    note.text = req.body.text || '';

    note.save((err, result) => {
      if (err) {
        return res.status(400).json({
          message: `Can't update COMPLETED field in DB. ${err}`,
        });
      }
      return res.status(200).json({
        message: `Success`,
      });
    });
  });
};


const deleteNoteById = async (req, res) => {
  const user = await User.findOne({
    _id: req.user._id,
  });

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  Note.findOne({
    _id: req.params.id,
    userId: req.user._id,
  }, (err, note) => {
    if (err || !note) {
      return res.status(400).json({
        message: `Note not exist. ${err}`,
      });
    }

    Note.deleteOne({

      _id: req.params.id,

    }, (err, result) => {
      if (err) {
        return res.status(400).json({
          message: `Can't delete Note from DB. ${err}`,
        });
      }

      return res.status(200).json({
        message: `Success`,
      });
    });
  });
};


module.exports = {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
};
