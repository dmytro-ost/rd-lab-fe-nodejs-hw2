const bcrypt = require('bcrypt');
const {
  User,
} = require('../models/userModel');


const getProfile = async (req, res) => {
  const user = await User.findOne({
    _id: req.user._id,
  }, {
    _id: 1,
    username: 1,
    createdDate: 1,
  });

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  res.status(200).json({
    'user': user,
  });
};


const changePass = async (req, res) => {
  const user = await User.findOne({
    _id: req.user._id,
  });

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  if (!(await bcrypt.compare(req.user.oldPassword, user.password))) {
    return res.status(400).json({
      message: `Wrong password!`,
    });
  }


  // All OK here. we can change password
  User.updateOne({

    _id: req.user._id,

  }, {

    password: await bcrypt.hash(req.user.newPassword, 10),

  }, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: `Can't update password in DB. ${err}`,
      });
    }

    return res.status(200).json({
      message: `Success`,
    });
  });
};


const deleteProfile = async (req, res) => {
  const user = await User.findOne({
    _id: req.user._id,
  });

  if (!user) {
    return res.status(400).json({
      message: `User not found!`,
    });
  }

  User.deleteOne({

    _id: req.user._id,

  }, (err, result) => {
    if (err) {
      return res.status(400).json({
        message: `Can't delete user from DB. ${err}`,
      });
    }

    return res.status(200).json({
      message: `Success`,
    });
  });
};


module.exports = {
  getProfile,
  deleteProfile,
  changePass,
};
