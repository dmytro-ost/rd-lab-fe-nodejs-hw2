const express = require('express');
const router = new express.Router();

const {
  asyncWrapper,
} = require('./helpers');

const {
  authMiddleware,
} = require('./middlewares/authMiddleware');

const {
  validPagination,
  validID,
} = require('./middlewares/noteMiddleware');


const {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
} = require('../controllers/notesController');


// Get user's noteS
router.get('/', authMiddleware, asyncWrapper(validPagination),
    asyncWrapper(getNotes));


// Add Note for User
// Joi check text string NOT REQ
router.post('/', authMiddleware, asyncWrapper(addNote));


// Get user's note by id
// ID STR REQUIRED
router.get('/:id', authMiddleware, asyncWrapper(validID),
    asyncWrapper(getNoteById));


// Update user's note by id
// ID STR REQUIRED
// STR not req
router.put('/:id', authMiddleware, asyncWrapper(validID),
    asyncWrapper(updateNoteById));


// Check/uncheck user's note by id
// ID STR REQUIRED
router.patch('/:id', authMiddleware, asyncWrapper(validID),
    asyncWrapper(checkNoteById));


// Delete user's note by id
// ID STR REQUIRED
router.delete('/:id', authMiddleware, asyncWrapper(validID),
    asyncWrapper(deleteNoteById));


module.exports = router;
