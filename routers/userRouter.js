const express = require('express');
const router = new express.Router();

const {
  asyncWrapper,
} = require('./helpers');

const {
  authMiddleware,
} = require('./middlewares/authMiddleware');

const {
  validOldNewPass,
} = require('./middlewares/changePassMiddleware');


const {
  getProfile,
  deleteProfile,
  changePass,
} = require('../controllers/profileController');


router.get('/me', authMiddleware, asyncWrapper(getProfile));

router.delete('/me', authMiddleware, asyncWrapper(deleteProfile));

router.patch('/me', authMiddleware, asyncWrapper(validOldNewPass),
    asyncWrapper(changePass));


module.exports = router;
