const Joi = require('joi');


module.exports.validCredential = async (req, res, next) => {
  const schema = Joi.object({
    username:
      Joi.string().required(),
    password:
      Joi.string().required(),
  });


  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }


  next();
};
