const Joi = require('joi');


const validPagination = async (req, res, next) => {
  const schema = Joi.object({
    offset:
      Joi.string().allow(null, '').pattern(/^\d*$/mi),
    limit:
      Joi.string().allow(null, '').pattern(/^\d*$/mi),
  });

  try {
    await schema.validateAsync(req.query, {
      allowUnknown: true,
    });

    req.note = {};

    req.note.offset = parseInt(req.query.offset) > 0 ?
      parseInt(req.query.offset) : 0;

    req.note.limit = parseInt(req.query.limit) > 0 ?
      parseInt(req.query.limit) : 0;

    next();
  } catch (err) {
    return res.status(400).json({
      message: `Pagination parameter(s) is not valid. ${err.message}`,
    });
  }
};


const validID = async (req, res, next) => {
  const schema = Joi.object({
    id:
      Joi.string().pattern(/^[a-f\d]{24}$/im).required(),
  });

  try {
    await schema.validateAsync(req.params);

    req.noteID = req.params.id;
    next();
  } catch (err) {
    return res.status(400).json({
      message: `Note ID is not valid. ${err.message}`,
    });
  }
};

module.exports = {
  validPagination,
  validID,
};
