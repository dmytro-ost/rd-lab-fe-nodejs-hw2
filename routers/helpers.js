const path = require('path');
const fs = require('fs');


/**
 * Wrapper to hide try/catch
 * @param {*} callback
 * @return {function} The function
 */
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
}


/**
 * @param {*} filePath
 * @return {Boolean}
 */
function ensureDirectoryExistence(filePath) {
  const dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}


/**
 * @param  {String} message='Error here!'
 * @param  {int} errorCode=500
 * @param  {String} super(message)
 */
class UnauthorizedError extends Error {
  /**
   * @param  {String} message='Error here!'
   * @param  {int} errorCode=500
   */
  constructor(message = 'Error here!', errorCode = 500) {
    super(message);
    this.statusCode = errorCode;
  }
}


module.exports = {
  asyncWrapper,
  ensureDirectoryExistence,
  UnauthorizedError,
};
