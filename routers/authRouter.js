const express = require('express');
const router = new express.Router();

const {
  asyncWrapper,
} = require('./helpers');


const {
  validCredential,
} = require('./middlewares/validationMiddleware');


const {
  login, register,
} = require('../controllers/authController');


router.post('/register', asyncWrapper(validCredential), asyncWrapper(register));

router.post('/login', asyncWrapper(validCredential), asyncWrapper(login));

module.exports = router;
