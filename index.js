require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const app = express();
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const notesRouter = require('./routers/notesRouter');
const PORT = process.env.PORT || 8080;
const LOG_DIR = process.env.LOG_DIR || '/log';

const {
  ensureDirectoryExistence,
} = require('./routers/helpers');


app.use(express.json());


// write log
const logPath = path.join(__dirname + LOG_DIR, 'access.log');
ensureDirectoryExistence(logPath); // create dir
const accessLogStream = fs.createWriteStream(logPath, {
  flags: 'a',
});
app.use(morgan('combined', {
  stream: accessLogStream,
}));


app.use('/api/auth', authRouter);


app.use('/api/users', userRouter);


app.use('/api/notes', notesRouter);


// 400 'Bad request' for all others query
app.all('*', (req, res) => {
  return res.status(400).json({
    message: `Bad request000.`,
  });
});


// catch errors
app.use((err, req, res, next) => {
  res.status(500).json({
    message: err.message,
  });
});


// db connect and server start
const start = async () => {
  await mongoose.connect('mongodb+srv://dmytro_ostretsov:1234567890@cluster0.yyfim.mongodb.net/db_hw2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
